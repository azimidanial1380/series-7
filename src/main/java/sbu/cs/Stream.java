package sbu.cs;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;

public class Stream {

    private final Socket socket;
    protected DataInputStream input;
    protected DataOutputStream output;
    protected String fileName ;

    // Client Constructor
    Stream(InetAddress acceptorHost, int acceptorPort) throws IOException {
        socket = new Socket(acceptorHost, acceptorPort);
        setStreams();
    }
    // Server Constructor
    Stream(Socket socket) throws IOException {
        this.socket = socket;
        setStreams();
    }

    private void setStreams() throws IOException {
        input = new DataInputStream(socket.getInputStream());
        output = new DataOutputStream(socket.getOutputStream());
    }

    public void close() throws IOException {
        socket.close();
    }

    public void sendFile(String filePath) throws IOException {
        setFileName(filePath) ;
        File file = new File(filePath);
        try(FileInputStream fileIn = new FileInputStream(file)){
            output.writeUTF(fileName);
            byte[] buf = new byte[Short.MAX_VALUE];
            int bytesRead;
            while ((bytesRead = fileIn.read(buf)) != -1) {
                output.writeShort(bytesRead);
                output.write(buf, 0, bytesRead);
            }
            output.writeShort(1);
        }
        catch (IOException e){
            throw new IOException("IO error occurred during sending file.", e);
        }

    }

    public void receiveFile(String directory) throws IOException {
        fileName = input.readUTF();
        directory += fileName ;
        File outFile = new File(directory);
        long fileLen = outFile.length() ;
        Server.LOGGER.log(Level.INFO, "File received: {0} bytes. ", fileLen);

        try(FileOutputStream fileOut = new FileOutputStream(outFile)){
            byte[] buf = new byte[Short.MAX_VALUE];
            int bytesSent;
            while( (bytesSent = input.readShort()) != 1 ) {
                input.readFully(buf,0,bytesSent);
                fileOut.write(buf,0,bytesSent);
            }
        }
        catch (IOException e){
            throw new IOException("IO error occurred during receiving file.",e) ;
        }
    }
    public void setFileName(String filePath){
        Path path = Paths.get(filePath);
        Path getFileName = path.getFileName();
        this.fileName =  getFileName.toString();
    }

}

