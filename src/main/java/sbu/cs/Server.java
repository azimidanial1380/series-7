package sbu.cs;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.logging.Logger ;

public class Server {

    private static final int SERVER_PORT= 65535; // default port
    public static final Logger LOGGER = Logger.getLogger(Server.class.getName());

    /**
     * Create a server here and wait for connection to establish,
     *  then get file name and after that get file and save it in the given directory
     *
     * @param args an string array with one element
     * @throws IOException ...
     */
    public static void main(String[] args) throws IOException {
        System.setProperty("java.util.logging.SimpleFormatter.format",
                "%1$tF %1$tT %4$s %2$s %5$s%6$s%n");
        // this thread should wait till client get filename from args and appoint it to filename
        String directory = args[0] + "/" ;     // default: "server-database"
        serverFileReceiver(directory);
    }

    private static void serverFileReceiver(String directory) throws IOException{
        try(ServerSocket myConnectionSocket = new ServerSocket(SERVER_PORT)) {
            LOGGER.info("Server is ready!");
            LOGGER.info("Waiting for a connection...");

            Stream dataSocket = new Stream(myConnectionSocket.accept());
            LOGGER.info("connection accepted");

            dataSocket.receiveFile(directory);

            LOGGER.info("Closing the Socket...");
            dataSocket.close();
            LOGGER.info("Socket closed!");
        }
        catch (IOException ex) {
            throw new IOException("IOException occurred ",ex) ;
        }
    }

}