package sbu.cs;

import java.net.InetAddress;
import java.util.logging.Logger;

public class Client {

    private static final String HOST_NAME = "localhost";
    private static final String PORT_NUMBER = "65535";
    private static final Logger LOGGER = Logger.getLogger(Client.class.getName());
    private static String filePath ;


    /**
     * Create a socket and connect to server then send fileName and after that send file to be saved on server side.
     * in here filePath == fileName
     *
     * @param args a string array with one element
     */
    public static void main(String[] args) {
        System.setProperty("java.util.logging.SimpleFormatter.format",
                "%1$tF %1$tT %4$s %2$s %5$s%6$s%n");
        filePath = args[0] ;      // "sbu.png" or "book.pdf"
        clientFileSender() ;
    }

    private static void clientFileSender(){
        try {
            Stream socket = new Stream(InetAddress.getByName(HOST_NAME), Integer.parseInt(PORT_NUMBER)) ;
            socket.sendFile(filePath);

            LOGGER.info("File sent!");
            socket.close();
            LOGGER.info("Socket closed!");
        }catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}

